﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Global
    {
        public static String VERSION = "1.0.0";
        public static String CMD_HANDSHAKE = "handshake";
        public static String CMD_HEART = "heart_beat";
        public static String PORT = "9011";
        public static String IP = "120.25.146.138";

        public static Dictionary<string, object> configs = null; 

        public static int seq = 1;

        public static int SEQ()
        {
            int v = seq;
            seq += 2;
            return v ;
        }
        public static Int64 GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        public static Request getHandshakeJson()
        {
            Request request = new Request();
            request.cmd = Global.CMD_HANDSHAKE;
            request.version = Global.VERSION;
            request.seq = Global.SEQ();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("serail_num", new SoftReg().getMNum());
            request.cmd_body = dict;
            return request;
        }
        public static Request getHeartJson()
        {
            Request request = new Request();
            request.cmd = Global.CMD_HEART;
            request.version = Global.VERSION;
            request.seq = Global.SEQ();
            return request;
        }
    }
    class Response
    {
        public int code;
        public String msg;
        public int seq;
        public Dictionary<string, object> resp_body;
    }
    class Request
    {
        private String Cmd;

        public String cmd
        {
            get { return Cmd; }
            set { Cmd = value; }
        }
        private String Version;

        public String version
        {
            get { return Version; }
            set { Version = value; }
        }
        private int Seq;

        public int seq
        {
            get { return Seq; }
            set { Seq = value; }
        }
        private Dictionary<string, object> Cmd_body;

        public Dictionary<string, object> cmd_body
        {
            get { return Cmd_body; }
            set { Cmd_body = value; }
        }
    }
}
